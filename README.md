# ansible_tutorial
ansible all --list-hosts
ansible all -m gather_facts
ansible all -m gather_facts --limit 192.168.0.222 | grep ansible_distribution
ansible all -m apt -a name=htop,lnav --become --ask-become-pass
ansible all -m apt -a name=tmux --become --ask-become-pass
ansible all -m apt -a update_cache=true --become --ask-become-pass
ansible all -m apt -a upgrade=dist --become --ask-become-pass
ansible all -m apt -a upgrade=yes --limit 192.168.0.248 --become --ask-become-pass
ansible all -m ping
ansible-playbook install_apache.yml --ask-become-pass
ansible-playbook install_favorites.yml --ask-become-pass
ansible-playbook remove_apache.yml --ask-become-pass
ansible-playbook upgrade.yml --ask-become-pass

192.168.0.248 apache_package=apache2 php_package=libapache2-mod-php
192.168.0.237 apache_package=apache2 php_package=libapache2-mod-php
192.168.0.200 apache_package=apache2 php_package=libapache2-mod-php
192.168.0.222 apache_package=httpd php_package=php